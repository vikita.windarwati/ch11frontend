import React, { Fragment, useState } from "react";
import NavbarComponent from "./NavbarComponent";
import CarouselComponents from "../components/CarouselComponents";
import GameItemComponent from "../components/GameItemComponent";
import FooterComponents from "../components/FooterComponents";
import { Container } from "react-bootstrap";

const Landing = (props) => {
  const [gameList] = useState([
    { id: 1, title: "Among Us" },
    { id: 2, title: "Mobile Legends" },
    { id: 3, title: "Call of Duty Mobile" },
    { id: 4, title: "Ghensin Impact" },
  ]);
  return (
    <>
      <NavbarComponent />
      <Container>
        <CarouselComponents props={props} />
        <section style={{ margin: "50px 20px" }}>
          <div style={{ paddingLeft: 20 }}>Hot Games</div>
          <div className="d-flex">
            {gameList.map((game) => (
              <Fragment key={game.id}>
                <GameItemComponent title={game.title} />
              </Fragment>
            ))}
          </div>
        </section>
      </Container>
      <FooterComponents />
    </>
  );
};

export default Landing;
