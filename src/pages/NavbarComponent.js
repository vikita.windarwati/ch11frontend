import React, { Fragment } from 'react'
import '../styles/Custom.css'
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap'

function NavbarComponent() {
  return (
    <Fragment>
      <Navbar className="binar-color py-3" variant="dark" expand="lg">
        <Container className="navbar-container">
          <Navbar.Brand href="/home">
            <img
              src={process.env.PUBLIC_URL + '/secondary-logo.png'}
              width="95"
              height="35"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto">
              <NavDropdown
                title="Profile"
                className="nav-font-size font-white font-nunito active"
                id="basic-nav-dropdown"
              >
                <NavDropdown.Item href="/profile">
                  Personal Info
                </NavDropdown.Item>
                <NavDropdown.Item href="/history">
                  Game History
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </Fragment>
  )
}

export default NavbarComponent
