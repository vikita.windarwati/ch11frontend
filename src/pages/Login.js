import React from 'react'
import LoginForm from './LoginForm'
import { Nav } from 'react-bootstrap'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import '../styles/auth.css'
import PropTypes from 'prop-types'
const LoginPage = ({ location }) => {
  return (
    <div className="container-landing">
      <ToastContainer />
      <div className="auth-container">
        <img
          src={process.env.PUBLIC_URL + '/secondary-logo.png'}
          className="auth-logo"
          alt="Binar Logo"
        />
        <div className="auth-card">
          <LoginForm location={location} />
        </div>
        <div className="auth-redirect">
          Belum punya akun? Daftar{' '}
          <span>
            <Nav.Link href="/register" className="auth-link-custom">
              disini
            </Nav.Link>
          </span>
        </div>
      </div>
    </div>
  )
}

LoginPage.propTypes = {
  location: PropTypes.string,
}
export default LoginPage
