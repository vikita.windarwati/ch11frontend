import React from 'react'
import NavbarComponent from './NavbarComponent'
import FooterComponents from '../components/FooterComponents'
import { Container, Row } from 'react-bootstrap'
import { useState } from 'react'
import '../styles/gameDetail.css'
import ReactPlayer from 'react-player'
import PropTypes from 'prop-types'

const GameDetail = (props) => {
  const [data] = useState({
    title: 'Paper Rock Scissors',
    genres: [
      { id: 1, label: 'Action' },
      { id: 2, label: 'Arcade' },
    ],
    description:
      'Di game ini Anda akan melawan computer. Ada 3 ronde pada game ini. Anda harus memilih batu / gunting / kertas di setiap rondenya. Hasil game akan ditentukan saat semua ronde telah selesai. ',
  })

  return (
    <div>
      <NavbarComponent />
      <Container className="container-game-detail">
        <Row>
          <div className="col-md-6 col-sm-12 game-col" id="game-picture">
            <div className="game-picture">
              <ReactPlayer
                url="https://youtu.be/XCU13mwc_R4"
                controls
                width="inherit"
                height="inherit"
              />
            </div>
          </div>
          <div className="col-md-6 col-sm-12 game-col">
            <div className="detail-container">
              <h2 className="game-title">{data.title}</h2>
              <div className="game-genres">
                {data.genres.map((genre) => (
                  <span key={genre.id} className={`label ${genre.label}`}>
                    {genre.label}
                  </span>
                ))}
              </div>
              <div className="game-description">{data.description}</div>
            </div>
            <button
              className="button-play"
              onClick={() => props.history.push('/game/lobbi')}
            >
              Play
            </button>
          </div>
        </Row>
        <Row className="nav-section">
          <div className="col-6 col-sm-4 col-md-3">
            <div className="nav-button">
              <div className="label-button">Previous</div>
              <div className="label-name">Among Us</div>
            </div>
          </div>
          <div className="col-6 col-sm-4 col-md-3 next">
            <div className="nav-button right">
              <div className="label-button">Next</div>
              <div className="label-name">Call of Duty</div>
            </div>
          </div>
        </Row>
      </Container>
      <FooterComponents />
    </div>
  )
}
GameDetail.propTypes = {
  history: PropTypes.object,
}
export default GameDetail
