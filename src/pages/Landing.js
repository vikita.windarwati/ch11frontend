import React from 'react'
import '../styles/landing.css'
import ButtonComponent from '../components/ButtonComponent'
import PropTypes from 'prop-types'
const Landing = (props) => {
  const handleLogIn = () => props.history.push('/home')
  const handleRegister = () => props.history.push('/register')
  const handleLeaderboard = () => props.history.push('/leaderboard')

  return (
    <div className="container-landing">
      <div className="d-flex justify-content-center align-items-center w-100 h-100">
        <div className="text-center align-middle">
          <h1 className="align-self-center mt-3 h1-color">
            Welcome to Binar Gaming
          </h1>
          <h5 className="align-self-center mt-2 h3-color">
            A simple game for completing Binar course challange
          </h5>
          <div className="text-center button mt-4">
            <ButtonComponent
              label="Login"
              type="yellow"
              onClick={handleLogIn}
            />
            <ButtonComponent
              label="Register"
              type="rose"
              onClick={handleRegister}
            />
          </div>
          <div className="text-center">
            <ButtonComponent
              label="Leaderboards"
              type="violet"
              onClick={handleLeaderboard}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

Landing.propTypes = {
  history: PropTypes.object,
}

export default Landing
