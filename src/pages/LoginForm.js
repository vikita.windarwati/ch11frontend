import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import auth from '../services/authService'
import PropsTypes from 'prop-types'
class LoginForm extends Component {
  state = {
    data: { username: '', password: '' },
    error: {},
  }

  handleSubmit = async (e) => {
    e.preventDefault()

    try {
      const { username, password } = this.state.data
      await auth.login(username, password)

      const { state } = this.props.location
      window.location = state ? state.referrer.pathname : '/home'
    } catch (ex) {
      if (ex.response && ex.response.status >= 400) {
        const errors = { ...this.state.errors }
        errors.username = ex.response.data.message
        this.setState({ errors })
      }
    }
  }

  handleChange = ({ currentTarget: input }) => {
    const data = { ...this.state.data }
    data[input.name] = input.value

    this.setState({ data })
  }

  render() {
    const { errors } = this.state
    if (auth.getCurrentUser()) return <Redirect to="/home" />

    return (
      <>
        <h4
          style={{ paddingBottom: 10, fontWeight: 700, color: `var(--binar)` }}
        >
          Login
        </h4>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <input
              className="form-control"
              id="username"
              name="username"
              type="text"
              onChange={this.handleChange}
              placeholder="Username"
              style={{ height: 44 }}
            />
            {errors && (
              <div className="invalid-feedback" style={{ display: 'block' }}>
                {errors.username}
              </div>
            )}
          </div>
          <div className="form-group">
            <input
              className="form-control"
              id="password"
              name="password"
              type="password"
              onChange={this.handleChange}
              placeholder="Password"
              style={{ height: 44 }}
            />
          </div>
          <button
            className="btn btn-primary"
            type="submit"
            style={{
              width: '100%',
              height: 44,
              backgroundColor: '#F9B91E',
              borderColor: '#F9B91E',
              borderRadius: 4,
            }}
          >
            Masuk
          </button>
        </form>
      </>
    )
  }
}

LoginForm.propTypes = {
  location: PropsTypes.object,
}
export default LoginForm
