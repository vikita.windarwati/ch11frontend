import React from 'react'
import '../styles/lobi.css'
import { Image, Button, Container } from 'react-bootstrap'
import PropTypes from 'prop-types'
function LobiPage(props) {
  return (
    <Container>
      <div className="center-body">
        <Image
          src={process.env.PUBLIC_URL + '/primary-logo.png'}
          className="img-logo"
        />
        <h1 className="font-size-h1 mt-5">Paper Rock Scissors</h1>
        <div className="mt-5">
          <Button className="start-button mr-5" variant="warning" href="/games">
            Start
          </Button>
          <Button
            className="start-button-history"
            variant="light"
            href="/game/history"
          >
            History
          </Button>
        </div>
        <div className="mt-10">
          <h5
            className="text-exit mt-5"
            onClick={() => props.history.push('/game-detail')}
          >
            Exit
          </h5>
        </div>
      </div>
    </Container>
  )
}
LobiPage.propTypes = {
  history: PropTypes.object,
}
export default LobiPage
