import React, { useState, useEffect, useRef } from 'react'
import { Container, Table } from 'react-bootstrap'
import Button from 'react-bootstrap/Button'
import '../styles/leaderboard.css'
import { useReactToPrint } from 'react-to-print';

function HighScore() {
  const [high, setHigh] = useState([])
  const componentRef = useRef();

  const getHigh = async () => {
    
      const response = await fetch(
        'https://pacific-taiga-53059.herokuapp.com/api/leaderboard'
      )
      const parseResponse = await response.json()
      console.log(high)
      setHigh(parseResponse)
    
  }
  useEffect(() => {
    getHigh()
  }, [])
  let value = 0;

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  
  return (
      <Container ref={componentRef}>
        <div className="text-center my-5">
          <div className="font-size-30">LEADERBOARD</div>
        </div>
        <div className="text-center mb-3">
        <Button onClick={handlePrint}>Print this out</Button>   
        </div>
        <Table striped bordered hover> 
          <thead>
            <tr>
              <th>No</th>
              <th>Username</th>
              <th>Score</th>
            </tr>
          </thead>
          <tbody>
            {high.map((data) => (
              <tr key={data.id}>
                <td>{(value = value + 1)}</td>
                <td>{data.username}</td>
                <td>{data.score}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
  )
}

export default HighScore
