import React, { Fragment, useState, useEffect } from 'react'
import { Container, Tabs, Tab } from 'react-bootstrap'
import NavbarComponent from './NavbarComponent'
import ProfileComponents from '../components/ProfileComponents'
import HistoryComponents from '../components/HistoryComponents'
import Axios from 'axios'
import { getJwt } from '../services/authService'
import PropTypes from 'prop-types'

function ProfilePage({ location }) {
  const [profile, setProfile] = useState([])
  const [histordata, setHistorydata] = useState([])

  const [key, setKey] = useState(
    location.pathname === '/profile' ? 'profile' : 'history'
  )
  const getProfile = async () => {
    Axios.get('https://pacific-taiga-53059.herokuapp.com/whoami', {
      headers: {
        Authorization: getJwt(),
      },
    })
      .then((response) => {
        setProfile(response.data)
      })
      .catch((error) => {
        console.log(error.response)
      })
  }

  const getPlayerLog = async () => {
    Axios.get('https://pacific-taiga-53059.herokuapp.com/api/player-logs', {
      headers: {
        Authorization: getJwt(),
      },
    })
      .then((response) => {
        setHistorydata(response.data)
      })
      .catch((error) => {
        console.log(error.response)
      })
  }

  useEffect(() => {
    getProfile()
    getPlayerLog()
  }, [])

  return (
    <Fragment>
      <NavbarComponent />
      <Container className="mt-5">
        <Tabs
          activeKey={key}
          onSelect={(k) => setKey(k)}
          id="uncontrolled-tab-example"
        >
          <Tab eventKey="profile" title="Profile">
            <Container>
              <div className="center-lg ">
                <ProfileComponents getprofile={profile} />
              </div>
            </Container>
          </Tab>
          <Tab eventKey="history" title="History">
            <div className="mt-5">
              <HistoryComponents gethsitory={histordata} />
            </div>
          </Tab>
        </Tabs>
      </Container>
    </Fragment>
  )
}
ProfilePage.propTypes = {
  location: PropTypes.object,
}

export default ProfilePage
