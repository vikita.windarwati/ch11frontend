import React from "react";
import { Button } from "react-bootstrap";

const RestartLeaderboardComponent = () => {
  return (
    <div className="text-center">
      <Button href="/game/lobbi">Finish Game</Button>
    </div>
  );
};

export default RestartLeaderboardComponent;
