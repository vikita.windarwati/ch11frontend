// import Axios from 'axios'
import axios from 'axios'
import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { useForm } from 'react-hook-form'
import '../styles/register.css'

const Register = () => {
  const { register, handleSubmit, setValue } = useForm()
  const onSubmit = async (formData, e) => {
    try {
      console.log(formData)
      const res = await axios.post(
        'https://pacific-taiga-53059.herokuapp.com/register',
        formData
      )

      console.log(res)
      console.log(res.data)

      if (res.status === 200) {
        alert(res.data.message)
        window.location = '/login'
      } else {
        alert(res.data.message)
        e.target.reset()
      }
    } catch (err) {
      alert(err)
    }
  }

  return (
    <div className="container-all">
      <img
        src={process.env.PUBLIC_URL + '/secondary-logo.png'}
        className="auth-logo"
        alt="Binar Logo"
      />
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 10,
        }}
      >
        <div
          style={{
            minWidth: 360,
            padding: 40,
            backgroundColor: '#EEF2F6',
            borderRadius: 8,
          }}
        >
          <h3>Daftar</h3>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="form-group">
              <input
                name="first_name"
                placeholder="Nama Depan"
                ref={register}
              />
              <input
                name="last_name"
                placeholder="Nama Belakang"
                ref={register}
              />
            </div>
            <div className="form-group">
              <input
                className="container"
                name="email"
                placeholder="Email"
                required={true}
                ref={register}
              />
            </div>
            <div className="form-group">
              <input
                className="container"
                name="username"
                placeholder="Username"
                required={true}
                ref={register}
              />
            </div>
            <div className="form-group">
              <input
                className="container"
                name="password"
                placeholder="Password"
                type="password"
                required={true}
                ref={register}
              />
            </div>
            <div className="form-group">
              <Row>
                <Col lg={7} md={9}>
                  <input
                    className="container"
                    name="profile_picture"
                    placeholder="Profile Picture URL"
                    type="text"
                    required={true}
                    ref={register}
                  />
                </Col>
                <Col lg={5} md={2}>
                  <button
                    className="btn container"
                    onClick={(e) => {
                      e.preventDefault()
                      setValue(
                        'profile_picture',
                        'https://sequelize.org/master/image/github.png'
                      )
                    }}
                  >
                    Use Default
                  </button>
                </Col>
              </Row>
            </div>

            <br></br>
            <input type="submit" className="btn container py-3" />
          </form>
        </div>
      </div>
      <p className="auth-redirect">
        Sudah punya akun? Login{' '}
        <span>
          <a href="/login" className="auth-link-custom">
            disini
          </a>
        </span>
      </p>
    </div>
  )
}

export default Register
