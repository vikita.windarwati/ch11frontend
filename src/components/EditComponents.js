/* eslint-disable */
import React, { Fragment, useState } from "react";
import { Button, Modal, Form, Row, Col, Image } from "react-bootstrap";
import Axios from "axios";
import "../styles/profile.css";
import { getJwt } from "../services/authService";

function EditComponents({ getAll }) {
  const [show, setShow] = useState(false);
  const [user, setUser] = useState(getAll);

  const onUserChange = (e) => {
    const { name, value } = e.target;
    setUser({ ...user, [name]: value });
  };
  const handleClose = () => {
    setShow(false);
    setUser(getAll);
  };
  const handleShow = () => {
    setShow(true);
    setUser(getAll);
  };

  const onSubmitted = async () => {
    let data = {
      first_name: user.first_name,
      last_name: user.last_name,
      username: user.username,
      email: user.email,
      profile_picture: user.profile_picture,
    };
    /* eslint-disable */
    Axios.put("https://pacific-taiga-53059.herokuapp.com/edit-player", data, {
      headers: {
        Authorization: getJwt(),
      },
    })
      .then((response) => {
        location.reload();
        window.location = "/profile";
      })
      .catch((error) => {
        console.log(error.response);
      });
    /* eslint-enable */
  };

  return (
    <Fragment>
      <Button variant="outline-binar btn-profile-binar" onClick={handleShow}>
        Edit Profile
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Edit Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Image
            src={user.profile_picture}
            rounded
            className="photo-profile my-3 body-center-img"
          />
          <Form>
            <Form.Group as={Row} controlId="formPlaintextFirstName">
              <Form.Label column sm="2">
                First Name
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  name="first_name"
                  value={user.first_name}
                  onChange={(e) => onUserChange(e)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} controlId="formPlaintextLastName">
              <Form.Label column sm="2">
                Last Name
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  name="last_name"
                  value={user.last_name}
                  onChange={(e) => onUserChange(e)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} controlId="formPlaintextLastName">
              <Form.Label column sm="2">
                Username
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  value={user.username}
                  name="username"
                  onChange={(e) => onUserChange(e)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} controlId="formPlaintextLastName">
              <Form.Label column sm="2">
                Email
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="email"
                  value={user.email}
                  name="email"
                  onChange={(e) => onUserChange(e)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} controlId="formPlaintextLastName">
              <Form.Label column sm="2">
                Photo
              </Form.Label>
              <Col sm="10">
                <Form.Control
                  type="text"
                  value={user.profile_picture}
                  name="profile_picture"
                  onChange={(e) => onUserChange(e)}
                />
              </Col>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={onSubmitted} type="submit">
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </Fragment>
  );
}

export default EditComponents;
