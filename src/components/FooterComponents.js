import React from "react";
import "../styles/footer.css";
const logo = process.env.PUBLIC_URL + "/primary-logo.png";

const FooterComponents = () => {
  return (
    <div className="footer">
      <div className="ml-5">
        <div className="row mb-5 mt-3">
          {/* column 1 */}
          <div className="col-6 ml-5">
            <img className="picsize mb-3" src={logo} alt="logo_binar" />
            <h1 className="list-unstyled style-list">
              <li className="mb-1">Pacific Century Place, SCBD Lt 99,</li>
              <li className="mb-3">
                Jl Jend Sudirman Kav. 1-100, Jakarta Selatan 12190
              </li>
              <li>Tel: +62 21 12128787</li>
            </h1>
          </div>
          {/* column 2 */}
          <div className="col">
            <h6 className="font-weight-bold color">Company</h6>
            <ul className="list-unstyled style-list">
              <li>Who we are</li>
              <li>Karir</li>
            </ul>
          </div>
          {/* column 3 */}
          <div className="col">
            <h6 className="font-weight-bold color">Platform</h6>
            <ul className="list-unstyled style-list">
              <li>Binar Mobile</li>
              <li>Binar PC</li>
            </ul>
          </div>
          {/* column 4 */}
          <div className="col">
            <h6 className="font-weight-bold color">Service</h6>
            <ul className="list-unstyled style-list">
              <li>Support</li>
              <li>Akun saya</li>
            </ul>
          </div>
        </div>
        <hr className="space-1" />
        <div className="row mt-4 ml-3 font-weight-light">
          <div className="col-9">
            <p className="col-sm">
              Copyright &copy; Binar Gaming. Trademarks belong to their
              respective owners. All rights reserved
            </p>
          </div>
          <div className="col">
            <p>Terms of service</p>
          </div>
          <div className="col">
            <p>Privacy Policy</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FooterComponents;
