/* eslint-disable */
import React from "react";
import "../styles/profile.css";
import { Card, Row, Col, Image } from "react-bootstrap";
import EditComponents from "./EditComponents";

const ProfileComponents = (props) => {
  const {
    username,
    email,
    last_name,
    first_name,
    profile_picture,
  } = props.getprofile;
  return (
    <div>
      <Card body className="binar-color-profile body-card-radius body-center">
        <Row>
          <Col className="text-center">
            <h1 className="font-size-20">Contact Info</h1>
            <Image
              src={profile_picture}
              rounded
              className="photo-profile my-3"
            />
          </Col>
          <Col className="my-5">
            <h1 className="font-size-20">{first_name + " " + last_name}</h1>
            <div className="mt-4 mb-5">
              <div className="d-flex align-items-center">
                <h1 className="font-size-16"> {username}</h1>
              </div>
              <div className="d-flex align-items-center">
                <h1 className="font-size-16"> {email}</h1>
              </div>
            </div>
            <div>
              <EditComponents getAll={props.getprofile} />
            </div>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default ProfileComponents;
