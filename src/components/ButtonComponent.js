import React from 'react'
import PropTypes from 'prop-types'
const ButtonComponent = ({ label, type, onClick }) => {
  return (
    <button
      className={`btn ${type}`}
      onClick={onClick}
      style={{ margin: 8, padding: 10 }}
    >
      {label}
    </button>
  )
}

ButtonComponent.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
}

export default ButtonComponent
