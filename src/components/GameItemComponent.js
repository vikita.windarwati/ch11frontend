import React from 'react'
import '../styles/gamecard.css'
import { Card, Button } from 'react-bootstrap'
import PropTypes from 'prop-types'
function GameItemComponent({ title }) {
  return (
    <div className="wrapper col-md-3" style={{ padding: 20 }}>
      <Card className="cardLoop">
        <Card.Body>
          <Card.Img className="cardImage" src="./profile2.png" />
          <Button className="cardButton" variant="secondary" size="sm">
            Play Now
          </Button>
        </Card.Body>
      </Card>
      <h5 className="cardTitle">{title}</h5>
    </div>
  )
}
GameItemComponent.propTypes = {
  title: PropTypes.string,
}
export default GameItemComponent
