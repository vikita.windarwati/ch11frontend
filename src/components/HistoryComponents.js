import React from 'react'
import { Table } from 'react-bootstrap'
import Moment from 'moment'
// import { useReactToPrint } from 'react-to-print';
import Button from 'react-bootstrap/Button'
import PDF from "react-to-pdf";

const ref = React.createRef();
const options = {
  orientation: 'landscape',
  // unit: 'in',
  // format: [4,2]
};

// import PropTypes from 'prop-types'
/* eslint-disable */
function HistoryComponents(props) {
  // const componentRef = useRef();

  const DateFormatter = (date) => {
    return Moment(date).format('DD MMMM YYYY, h:mm a')
  }
  
  // const handlePrint = useReactToPrint({
  //   content: () => componentRef.current,
  // });

  return (
    <div>
      <div className='mb-4'>
      {/* <div ref=componentRef> */}
        {/* <Button className="mt-3 mb-3"onClick={handlePrint}>Print this out</Button>    */}
        <PDF targetRef={ref} filename="score-history.pdf" options={options} x={13.5} y={13.5} scale={0.9}>
          {({toPdf}) => (
              <Button onClick={toPdf}>Generate pdf</Button>
          )}
        </PDF>
      </div>
      <div ref={ref}>
        <Table striped bordered hover className="binar-color-profile">
          <thead>
            <tr>
              <th>Date</th>
              <th>Game</th>
              <th>Score</th>
              <th>Result</th>
            </tr>
          </thead>
          <tbody>
            {props.gethsitory
              .map((x) => (
                <tr
                  key={x.id}
                  className={
                    x.score >= 3
                      ? 'background-win'
                      : x.score === 0
                      ? 'background-lose'
                      : ''
                  }
                >
                  <td>{DateFormatter(x.createdAt)}</td>
                  <td>{x.game}</td>
                  <td>{x.score}</td>
                  <td>
                    {x.score >= 3 ? 'Menang' : x.score >= 1 ? 'Draw' : 'Kalah'}
                  </td>
                </tr>
              ))
              .reverse()}
          </tbody>
        </Table>
      </div>
    </div>
  )
}

// HistoryComponents.propTypes = {
//   gethsitory: PropTypes.array,
// }
export default HistoryComponents
