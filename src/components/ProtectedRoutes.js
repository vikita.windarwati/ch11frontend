import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import auth from '../services/authService'
/* eslint-disable */
function ProtectedRoutes({ path, component: Component, render, ...rest }) {
  return (
    <Route
      path={path}
      {...rest}
      render={(props) => {
        if (!auth.getJwt())
          return (
            <Redirect
              to={{ pathname: '/login', state: { referrer: props.location } }}
            />
          )

        return Component ? <Component {...props} /> : render(props)
      }}
    />
  )
}

export default ProtectedRoutes
