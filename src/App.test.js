/* eslint-disable */
import React from "react";
import { configure, mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { render, cleanup } from "@testing-library/react";

import GameDetail from "./pages/GameDetail";
import HomePage from "./pages/HomePage";
import Login from "./pages/Login";
import Landing from "./pages/Landing";

afterEach(cleanup);
configure({ adapter: new Adapter() });

it("Home Page is rendered successfully", () => {
  const wrapper = shallow(<HomePage />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

it("Game Detail page title is Paper Rock Scissors", () => {
  const { getByText } = render(<GameDetail />);
  expect(getByText(/Paper/i).textContent).toBe("Paper Rock Scissors");
});

describe("Landing Page", () => {
  it("should rendered successfully", () => {
    const wrapper = shallow(<Landing />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
  describe("contains Button Login", () => {
    it("should push history props to Home", () => {
      const wrapper = mount(<Landing />);
      console.log(wrapper.props());
    });
  });
});

describe("Login Page", () => {
  it("should rendered successfully", () => {
    const wrapper = shallow(<Login />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
