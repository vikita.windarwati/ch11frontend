import http from "./httpService";
import { apiUrl } from "../config/config.json";
import jwtDecode from "jwt-decode";

const endPoint = `${apiUrl}/login`;
const tokenKey = "binar.access.token";
const refreshKey = "binar.refresh.token";

http.setJwt(getJwt());

export async function login(username, password) {
  const { data } = await http.post(endPoint, {
    username: username,
    password: password,
  });

  localStorage.setItem(tokenKey, data.accessToken);
  localStorage.setItem(refreshKey, data.refreshToken);
}

export function logout() {
  localStorage.removeItem(tokenKey);
  localStorage.removeItem(refreshKey);
}

export function getCurrentUser() {
  try {
    const token = localStorage.getItem(tokenKey);
    return jwtDecode(token);
  } catch (err) {
    return null;
  }
}

export function getJwt() {
  return localStorage.getItem(tokenKey);
}

const auth = {
  login,
  logout,
  getCurrentUser,
  getJwt,
};

export default auth;
