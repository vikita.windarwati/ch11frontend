import { Route, Switch } from "react-router-dom";
import React, { Component } from "react";
import RegisterPage from "./pages/Register";
import LoginPage from "./pages/Login";
import ProfileComponent from "./pages/ProfilePage";
import Logout from "./pages/Logout";
import ProtectedRoutes from "./components/ProtectedRoutes";
import HomePage from "./pages/HomePage";
import Landing from "./pages/Landing";
import HighScore from "./pages/HighScore";
import Games from "./pages/Games";
import GameDetail from "./pages/GameDetail";
import LobiPage from "./pages/LobiPage";
import HistoryAfter from "./pages/HistoryAfterGame";

class App extends Component {
  render() {
    return (
      <div className="font-nunito">
        <Switch>
          <Route path="/" exact component={Landing} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/login" component={LoginPage} />
          <Route path="/logout" component={Logout} />
          <ProtectedRoutes path="/profile" component={ProfileComponent} />
          <ProtectedRoutes path="/history" component={ProfileComponent} />
          <ProtectedRoutes path="/game-detail" component={GameDetail} />
          <ProtectedRoutes path="/home" component={HomePage} />
          <ProtectedRoutes path="/game/lobbi" component={LobiPage} />
          <ProtectedRoutes path="/game/history" component={HistoryAfter} />
          <ProtectedRoutes path="/games" component={Games} />
          <Route path="/leaderboard" component={HighScore} />
        </Switch>
      </div>
    );
  }
}

export default App;
